#include "../include/omnibase.h"

#include <Eigen/Geometry>

#include <iostream>

// ROS Kinetic: boost::shared_ptr<urdf::Model> URDFModelPtr;
// ROS Lunar and newer: urdf::ModelSharedPtr URDFModelPtr;
// Fix from:
// https://github.com/ros-planning/moveit/issues/506
// https://github.com/ros-planning/moveit/pull/542/commits/35bd0f054ba80e18b4f5bb7f7eafd83fc1c175f5

// typedef boost::shared_ptr<urdf::Model> URDFModelPtr;
// typedef boost::shared_ptr<srdf::Model> SRDFModelPtr;
typedef urdf::ModelSharedPtr URDFModelPtr;
typedef srdf::ModelSharedPtr SRDFModelPtr;

OmniBase::OmniBase(const std::string &name, const std::string &path_urdf, const std::string &path_srdf)
    : OmniBase::OmniBase(name, path_urdf , path_srdf , 30)
// OmniBase::OmniBase(const std::string &path_urdf, const std::string &path_srdf)
    // : OmniBase::OmniBase(path_urdf , path_srdf , 30)
{
}

OmniBase::OmniBase(const std::string &name, const std::string &path_urdf, const std::string &path_srdf, double vel_filter_wc):
// OmniBase::OmniBase(const std::string &path_urdf, const std::string &path_srdf, double vel_filter_wc):
    last_published_joint5_velocity(0),
    velocity_filter_wc(vel_filter_wc),
    name(name),
    enable_force_flag(false)
{
    node = ros::NodeHandlePtr( new ros::NodeHandle("") );

    // Prepare joint state publisher.
    // topic_name = name + "joint_states";
    topic_name = "joint_states";
    pub_joint = node->advertise<sensor_msgs::JointState>(topic_name, 10);

    // Prepare pose publisher.
    // topic_name = name + "pose";
    topic_name = "pose";
    pub_pose = node->advertise<geometry_msgs::PoseStamped>(topic_name, 10);
    
    // Prepare twist publisher.
    // topic_name = name + "twist";
    topic_name = "twist";
    pub_twist = node->advertise<geometry_msgs::TwistStamped>(topic_name, 10);

    // Prepare button state publisher and subscribe it.
    // topic_name = name + "button_state";
    topic_name = "button_state";
    pub_button = node->advertise<omni_msgs::OmniButtonEvent>(topic_name, 10);

    // Subscribe joint_torque topic.
    // topic_name = name + "in/joint_torque";
    topic_name = "in/joint_torque";
    sub_torque = node->subscribe(topic_name, 1, &OmniBase::torqueCallback, this);

    // Subscribe enable_control topic.
    // topic_name = name + "in/enable_control";
    topic_name = "in/enable_control";
    sub_enable_control = node->subscribe(topic_name, 1, &OmniBase::enableControlCallback, this);

    // Subscribe force_feedback topic.
    // topic_name = name + "in/force";
    topic_name = "in/force";
    sub_force = node->subscribe(topic_name, 1, &OmniBase::forceFeedbackCallback, this);

    ros::param::param<std::string>("~tf_prefix",tf_prefix,"");
    if (!tf_prefix.empty()) { tf_prefix += "/"; }

    ros::param::param<double>("~force_gain",force_gain,1.0);

    // Should be the same as urdf/srdf file
    links.base.name = "base";
    links.torso.name = "torso";
    links.upper_arm.name = "upper_arm";
    links.lower_arm.name = "lower_arm";
    links.wrist.name = "wrist";
    links.tip.name = "tip";
    links.stylus.name = "stylus";
    links.end_effector.name = "end_effector";

    std::memset(last_buttons, 0, sizeof(last_buttons));

    // Initializing robot_model for MoveIt!
    URDFModelPtr urdf_model = URDFModelPtr( new urdf::Model() );
    urdf_model->initFile(path_urdf);
    SRDFModelPtr srdf_model = SRDFModelPtr( new srdf::Model() );
    srdf_model->initFile(*urdf_model, path_srdf);

    kinematic_model = robot_model::RobotModelPtr( new robot_model::RobotModel(urdf_model, srdf_model) );

    ROS_INFO("Model frame: %s", kinematic_model->getModelFrame().c_str());
    kinematic_state = robot_state::RobotStatePtr(new robot_state::RobotState(kinematic_model));
    kinematic_state->setToDefaultValues();
    joint_model_group = kinematic_model->getJointModelGroup("all");
    if (!joint_model_group)
    {
        // TODO - quit program
    }
    state.joint_names = joint_model_group->getJointModelNames();

    const int n = state.joint_names.size();
    joint_state.name.resize(n);
    joint_state.position.resize(n);
    joint_state.velocity.resize(n);
    joint_state.effort.resize(n);
    for (int k = 0; k < n; ++k)
    {
        joint_state.name[k] = state.joint_names[k];
    }
}

void OmniBase::torqueCallback(const geometry_msgs::Vector3::ConstPtr & msg)
{
    std::vector<double> input = {msg->x, msg->y, msg->z};
    setTorque(input);
}

void OmniBase::enableControlCallback(const std_msgs::Bool::ConstPtr & msg)
{
    enableControl(msg->data);
    if (msg->data) {
        ROS_INFO("Enable Control = true");
    } else {
        ROS_INFO("Enable Control = false");
    }
}

void OmniBase::forceFeedbackCallback(const geometry_msgs::WrenchStamped::ConstPtr& wrenchStamped)
{
    std::string force_frame = wrenchStamped->header.frame_id;
    // Optoforce is not precise enough on all axis. So should only consider (0, force.y, 0).
    Eigen::Vector3d force_vector(wrenchStamped->wrench.force.x, wrenchStamped->wrench.force.y, wrenchStamped->wrench.force.z);
    
    Eigen::Vector3d joint_torques = OmniBase::calculateTorqueFeedback(force_vector, force_frame);
    std::vector<double> torque_input(3);
    std::copy(joint_torques.data(), joint_torques.data() + 3, torque_input.begin());
    this->setTorque(torque_input);
}

Eigen::Vector3d OmniBase::calculateTorqueFeedback(const Eigen::Vector3d& force, const std::string& force_frame)
{
    // Get the desired frame
    auto frame = links.tip.name; // links.lower_arm.name;

    // Get the Jacobian matrix written on the base frame
    // Eigen::Vector3d origin(0,0.08,0); // This messes up force direction for some reason
    Eigen::Vector3d origin(0.0, 0.0, 0.0);
    Eigen::MatrixXd jacobian;
    auto link_model = kinematic_state->getLinkModel(frame);
    kinematic_state->getJacobian(joint_model_group, link_model, origin, jacobian, false); // jacobian = (J_frame)base
    Eigen::Matrix3d Jpos = jacobian.block<3,3>(0,0);

    // For some reason, state.position here is not correct (keeps returning 0, -110 -35).
    // So end-effector trasnformation calculated from moveit fwdkin
    // Eigen::Affine3d end_effector_state;
    // end_effector_state = kinematic_state->getGlobalLinkTransform(frame); // getGlobalLinkTransform(links.end_effector.name)
    // Eigen::Vector3d pbe = end_effector_state.translation();
    // Eigen::Matrix3d Rbe = end_effector_state.rotation();

    Eigen::Vector3d torque;

    torque = Jpos.transpose() * force; // Force in base coord frame
    // torque = Jpos.transpose() * Rbe * force; // Force in tip coord frame
    
    return torque;
}

void OmniBase::calculateJointVelocities()
{
    double dt = 1/50.0;     // dt = 1/(ros::Rate); // ros::Rate defined in omninode.cpp

    for (int i = 0; i < 6; ++i)
    {
        state.vel_error[i] = state.angles[i] - state.vel_z[i];
        state.velocities[i] = velocity_filter_wc * state.vel_error[i];
        state.vel_z[i] += dt * velocity_filter_wc * state.vel_error[i];
    }
}

void OmniBase::updateRobotState()
{
    Eigen::VectorXd joint_angles(6);
    joint_angles << state.angles[0],
            state.angles[1],
            state.angles[2],
            state.angles[3],
            state.angles[4],
            state.angles[5];
    kinematic_state->setJointGroupPositions(joint_model_group, joint_angles);

    Eigen::VectorXd joint_velocities(6);
    joint_velocities << state.velocities[0],
            state.velocities[1],
            state.velocities[2],
            state.velocities[3],
            state.velocities[4],
            state.velocities[5];
    kinematic_state->setJointGroupVelocities(joint_model_group,joint_velocities);
}

void OmniBase::fwdKin()
{
    Eigen::Affine3d end_effector_state;
    end_effector_state = kinematic_state->getGlobalLinkTransform(links.tip.name);
    Eigen::Quaterniond quat(end_effector_state.rotation());
    Eigen::Vector3d pos = end_effector_state.translation();

    state.orientation[0] = quat.w();
    state.orientation[1] = quat.x();
    state.orientation[2] = quat.y();
    state.orientation[3] = quat.z();

    state.position[0] = pos[0];
    state.position[1] = pos[1];
    state.position[2] = pos[2];
}

void OmniBase::calculateEffectorVelocities()
{
    Eigen::Vector3d origin(0,0,0);
    Eigen::MatrixXd jacobian(6,6);

    // To use the pen position  use links.end_effector.name   instead of wrist position   links.tip.name 
    // auto current_end_effector_link_model = kinematic_state->getLinkModel(links.tip.name);  // Consider the wrist frame (intersection of joint 4-5-6). Does not consider joint 6 motion
    auto current_end_effector_link_model = kinematic_state->getLinkModel(links.stylus.name);  // Considre the stylus frame (point)
    kinematic_state->getJacobian(joint_model_group, current_end_effector_link_model, origin, jacobian, false);

    Eigen::VectorXd thetaDot(6);

    thetaDot(0) = state.velocities[0];
    thetaDot(1) = state.velocities[1];
    thetaDot(2) = state.velocities[2];
    thetaDot(3) = state.velocities[3];
    thetaDot(4) = state.velocities[4];
    thetaDot(5) = state.velocities[5];

    Eigen::VectorXd xDot(6);

    xDot = jacobian * thetaDot;

    for (int i=0; i<6; ++i)
    {
        state.twist[i] = xDot(i);
    }
}

void OmniBase::publishOmniState()
{
    if (!this->connected() /*|| !this->calibrated()*/)
    {
        // Phantom Omni is not open or calibrated. Don't publish.
        return;
    }

    // state.angles updated at high rate

    calculateJointVelocities(); // depends on state.angles
    // SET: state.angles and state.velocities

    updateRobotState(); // depends on state.angles and state.velocities
    // SET: moveit angles + velocities

    fwdKin(); // depends on moveit angles + velocities
    // SET: state.position and state.orientation

    calculateEffectorVelocities(); // depend on moveit jacobian and state.velocities
    // SET: state.twist
    
    // Publish the joint state.
    joint_state.header.stamp = ros::Time::now();
    for (int i = 0; i < 6; ++i)
    {
        joint_state.position[i] = state.angles[i]; // state.angles[i] + joint_states_offsets[i];
        joint_state.velocity[i] = state.velocities[i]; // joint_states_gain[i] * state.velocities[i];
    }
    pub_joint.publish(joint_state);

    // Publish the end effector pose.
    pose_stamped.header.stamp = ros::Time::now();
    pose_stamped.header.frame_id = tf_prefix + links.tip.name;
    pose_stamped.pose.position.x = state.position[0];
    pose_stamped.pose.position.y = state.position[1];
    pose_stamped.pose.position.z = state.position[2];
    pose_stamped.pose.orientation.w = state.orientation[0];
    pose_stamped.pose.orientation.x = state.orientation[1];
    pose_stamped.pose.orientation.y = state.orientation[2];
    pose_stamped.pose.orientation.z = state.orientation[3];
    pub_pose.publish(pose_stamped);

    // Publish the button event.
    std::vector<bool> button_state;
    this->getButtonsState(button_state);
    button_event.grey_button = button_state[0];
    button_event.white_button = button_state[1];
    button_event.grey_button_clicked = !last_buttons[0] && button_state[0];
    button_event.white_button_clicked = !last_buttons[1] && button_state[1];
    last_buttons[0] = button_state[0];
    last_buttons[1] = button_state[1];
    pub_button.publish(button_event);

    // Publish the twist.
    twist.header.stamp = ros::Time::now();
    twist.header.frame_id = tf_prefix + links.base.name;
    twist.twist.linear.x = state.twist[0];
    twist.twist.linear.y = state.twist[1];
    twist.twist.linear.z = state.twist[2];
    twist.twist.angular.x = state.twist[3];
    twist.twist.angular.y = state.twist[4];
    twist.twist.angular.z = state.twist[5];
    pub_twist.publish(twist);    

    // Check if the device is frozen. This usually happens when the read buffer
    // is too small and it is completely filled (at least that is what we think).
    if (state.velocities[5] == last_published_joint5_velocity)
    {
        if (++state.freeze_count > MAX_FREEZE_COUNT)
        {
            state.freeze_count = 0;
            this->wakeup();
        }
    }
    else
    {
        state.freeze_count = 0;
    }
    last_published_joint5_velocity = state.velocities[5];
}
